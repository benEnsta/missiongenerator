from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QFileDialog

# Initialize Qt resources from file resources.py
from .resources import *
# Import the code for the dialog
from .mission_generator_dialog import mission_genratorDialog
import os.path

from qgis.core import QgsMessageLog, Qgis, QgsVectorLayer, QgsField, QgsFeature, QgsGeometry, QgsPointXY
from qgis.core import QgsProject
from qgis.core import QgsDistanceArea, QgsUnitTypes
from qgis.PyQt.QtCore import QVariant
from qgis.core import QgsFeatureRequest
from qgis.gui import QgsRubberBand
from qgis.PyQt.QtWidgets import (QWidget, QPushButton, QLineEdit, 
    QInputDialog)
from qgis.utils import iface

import math


from .library.location.latitude import Latitude
from .library.location.longitude import Longitude

from .library.positioning.speed import Speed

from .library.mission.objects.point2d import Point2D

from .library.mission.common import *
from .library.mission.mission import Mission
from .library.mission.phases.ascent import Ascent
from .library.mission.phases.circle import Circle
from .library.mission.phases.dive import Dive
from .library.mission.phases.waypoint import Waypoint
from .library.mission.phases.waypoints import Waypoints
from .library.mission.phases.track import Track
from .library.mission.phases.path import Path


class MissionVectorLayer(QgsVectorLayer):
    def __init__(self, *args, **kwargs):
        QgsVectorLayer.__init__(self, *args, **kwargs)
        # self.vl_ghost = QgsVectorLayer("LineString", "Mission_ghost", "memory")
        qml_filename = os.path.join(os.path.dirname(__file__), 'qml_files/style_mission_layer_v2.qml')
        
        self.loadNamedStyle(qml_filename)
        self.first_start = False

        self.rb = QgsRubberBand(iface.mapCanvas(), False)
        self.rb.setWidth(2)
        self.layerModified.connect(self.update_ghost)
        self.willBeDeleted.connect(self.removeGhost)


    def __del__(self): 
        print('Destructor called, remove rb')
        self.rb.reset()
        iface.mapCanvas().scene().removeItem(self.rb)
    # Deleting (Calling destructor) 
    def removeGhost(self): 
        print('Destructor called, remove rb')
        self.rb.reset()
        iface.mapCanvas().scene().removeItem(self.rb)
        # QgsProject.instance().removeMapLayers( [self.vl_ghost.id()] )
        

    @classmethod
    def newMemoryLayer(self):
        vl = MissionVectorLayer("LineString", "Mission", "memory")
        pr = vl.dataProvider()

        # add fields
        pr.addAttributes([  QgsField("mode",  QVariant.String),
                            QgsField("depth", QVariant.Double),
                            QgsField("altitude", QVariant.Double),
                            QgsField("speed", QVariant.Double),
                            QgsField("order", QVariant.Int),
                            QgsField("bearing", QVariant.Double),
                            QgsField("length", QVariant.Double)])
                            
                            # QgsField("altitude", QVariant.Double)])
        vl.updateFields() # tell the vector layer to fetch changes from the provider
        vl.updateExtents()
        return vl

    def update_ghost(self):
        geoms = []
        for f in sorted(self.getFeatures(), key=lambda f: f['order']):
            geoms.extend(f.geometry().asPolyline())
        
        self.rb.reset()
        self.rb.setToGeometry(QgsGeometry.fromPolylineXY(geoms))
        self.rb.setWidth(3)
        self.rb.show()
        # self.rb.updatePosition()

    def changeLineDirection(self, featureId):
        f = self.getFeature(featureId)
        f.setGeometry(self.reverseLineDirection(f.geometry()))
        self.updateFeature(f)


    def swapOrder(self, f1, f2):
        # pr = self.dataProvider()
        # order_idx = self.fields().indexFromName("order")
        # print(order_idx)
        # # f1, f2 = pr.getFeatures([featureId1, featureId2])
        # pr.changeAttributeValues({
        #     feature1.id(): {order_idx: 0*feature2["order"]},
        #     feature2.id(): {order_idx: 0*feature1["order"]},
        # })

        # print(feature1["order"])
        # print(feature2["order"])
        # pr.commitChanges()
        self.beginEditCommand("Swap Feature order")
        f1["order"], f2["order"] = f2["order"], f1["order"]
        self.updateFeature(f1)
        self.updateFeature(f2)
        self.endEditCommand()
        # self.commitChanges()
        # self.triggerRepaint()
        
    
    def addFeatureWithOrder(self, new_feature):
        # pr = self.dataProvider()
        if self.featureCount() == 0:
            max_order = 0
        else:    
            max_order = max([f['order'] for f in self.getFeatures()])
        new_feature["order"] = max_order+1
        # fid_idx = self.fields().indexFromName("fid")
        # if fid_idx >= 0:
            # max_fid = max([f['fid'] for f in self.getFeatures()])
            # new_feature['fid'] = max_fid+1
        self.addFeature(new_feature)
        return max_order+1

    def moveOrder(self, old_order_value, new_order_value):
        # pr = self.dataProvider()
        self.layerModified.disconnect(self.update_ghost)
        order_idx = self.fields().indexFromName("order")
        max_order = max([f['order'] for f in self.getFeatures()])
        features = [[f.id(), f['order']] for f in self.getFeatures()]
        features = list(sorted(features, key=lambda f:f[1]))
        # print(f"move {old_order_value} -> {new_order_value}")
        # print(features)
        features.insert(new_order_value-1, features.pop(old_order_value-1))
        for i in range(len(features)):
            features[i][1] = i+1

        for fid, order in features:
            self.changeAttributeValue(fid, order_idx, order)
        self.layerModified.connect(self.update_ghost)
        # update = {fid: {order_idx: order} for (fid, order) in features}
        # self.dataProvider().changeAttributeValues(update)

    def insertFeatureAfter(self, new_feature, feature_order):
        """ interset feature new_feature at position  feature_order
        """

        order = self.addFeatureWithOrder(new_feature)
        self.moveOrder(int(order), int(feature_order))


    def reverseLineDirection(self, geom):
        return QgsGeometry.fromPolylineXY(geom.asPolyline()[::-1])

    
    def genParallelLines(self, featureId, nbrOfTrack=10, dist=100, side="portside"):
        side_angle = -90 if side == "portside" else 90
        f = self.getFeature(featureId)
        bearinf = f["bearing"]
        order0 = f["order"]
        geom = f.geometry().asPolyline()
        self.startEditing()
        # TODO Check geometry validity
        # print(dist, nbrOfTrack)
        distanceArea = QgsDistanceArea()
        d = QgsDistanceArea()
        d.setEllipsoid("WGS84")
        # crs = QgsCoordinateReferenceSystem.createFromString("EPSG:4326")
        # dist = d.measureLine(geom[0], geom[1])
        # print(d.ellipsoidCrs())
        # print(d.convertLengthMeasurement(dist, QgsUnitTypes.DistanceMeters))
        dist_deg = d.convertLengthMeasurement(dist, QgsUnitTypes.DistanceDegrees)
        fid_idx = self.fields().indexFromName("fid")
        for i in range(1, nbrOfTrack):
            f2 = QgsFeature(f)
            if fid_idx >= 0:
                f2.setAttribute(fid_idx, None)

            # feat.setAttributes(list(self.layer.getFeatures())[ind].attributes())
            newGeom = QgsGeometry(f.geometry())
            newGeom.translate(
                i*dist_deg*math.sin(math.radians(bearinf-side_angle)),
                i*dist_deg*math.cos(math.radians(bearinf-side_angle))
            )
            # print(newGeom.asPolyline())
            if i % 2 == 1:
                newGeom = QgsGeometry.fromPolylineXY(newGeom.asPolyline()[::-1])
            f2.setGeometry(newGeom)
            # f2["order"] += 
            self.insertFeatureAfter(f2, int(order0+i))
        self.updateExtents()
        # self.commitChanges()
        self.triggerRepaint()
        
    def genParallelLinesGUI(self, featureId):
        
        # self.startEdditing()
        # TODO Check geometry validity
        # ex = Example()
        # ex.exec_()
        # ex.show()
        """Run method that performs all the real work"""

        # Create the dialog with elements (after translation) and keep reference
        # Only create GUI ONCE in callback, so that it will only load when the plugin is started
        dlg = mission_genratorDialog()
    
        
        # show the dialog
        dlg.show()
        # Run the dialog event loop
        result = dlg.exec_()
        # See if OK was pressed
        if result:
            self.beginEditCommand("Feature triangulation")
            dist = dlg.distanceBetweenTracks.value()
            nbrOfTrack = dlg.nbrOfTrack.value()
            side = "portside" if dlg.rbtn_portside.isChecked() else "starboard"
            self.genParallelLines(featureId, nbrOfTrack, dist, side=side)
            self.endEditCommand()
            

    def addWaypoints(self, phase):
        feature = QgsFeature(self.fields())
        feature['speed'] = phase.get_speed()
        feature['depth'] = phase.get_depth()
        feature['altitude'] = 0
        feature['mode'] = "TRANSIT"
        print(feature)
        geom = []
        for pts in phase.get_points():
            lat, lon = pts.get_position()
            geom.append(QgsPointXY(lon.get_dd_value(), lat.get_dd_value()))
        feature.setGeometry(QgsGeometry.fromPolylineXY(geom))
        self.addFeatureWithOrder(feature)

    
    def addTrack(self, phase):
        feature = QgsFeature(self.fields())
        feature['speed'] = phase.get_speed()
        feature['altitude'] = phase.get_altitude()
        feature['depth'] = 0
        feature['mode'] = "TRACK"
        start_point = phase.start_point.get_position()
        start_point = QgsPointXY(start_point[1].get_dd_value(), start_point[0].get_dd_value())

        end_point = phase.end_point.get_position()
        end_point = QgsPointXY(end_point[1].get_dd_value(), end_point[0].get_dd_value())
        feature.setGeometry(QgsGeometry.fromPolylineXY([start_point, end_point]))
        self.addFeatureWithOrder(feature)


    def load(self, mission_filename):
        self.startEditing()
        mission = Mission()
        # Update object from json file
        mission.updatefromfile(mission_filename)
        print('Parse phases mission -> ')
        for phase in mission.get_phases():
            if isinstance(phase, Track):
                print('Track')
                self.addTrack(phase)
            if isinstance(phase, Waypoint):
                print('Waypoint not supported')
                # assert phase.get_name() == 'Waypoint_00' or 'Waypoint_01'
            if isinstance(phase, Waypoints):
                print('Waypoints')
                self.addWaypoints(phase)
        print('layer update')
        self.updateExtents()      
        self.commitChanges()   
    def export(self, filename, mission_name='Brest_00'):

        mission = Mission(mission_name)
        Ltrack = []
        for f in sorted(self.getFeatures(), key=lambda f: f['order']):
            if f["mode"] == 'TRACK':
                # TODO check number of points
                pts = f.geometry().asPolyline()
                start_pt, end_pt = pts[0], pts[1]
                # Add TRACK phase to mission
                start_point = Point2D(f'TP_start_{f["order"]}', Latitude(start_pt[1]), Longitude(start_pt[0]))
                end_point = Point2D(f'TP_stop_{f["order"]}', Latitude(end_pt[1]), Longitude(end_pt[0]))
                track = Track(f'Track_{f["order"]}', start_point, end_point, Speed(f["speed"]), f["altitude"])
                Ltrack.append(track)
                # print(len(Ltrack))
            elif len(Ltrack) > 0:
                # print("dump track")
                mission.add_path(Path("Path", tracks=Ltrack))
                Ltrack = []
            
            if f["mode"] == 'ASCENT':
                mission.add_ascent(Ascent())

            elif f['mode'] == "DIVE":
                mission.add_dive(Dive(f['depth']))

            elif f["mode"] == 'TRANSIT':
                l_point = []        
                for i,pts in enumerate(f.geometry().asPolyline()):
                    # Add WAYPOINT phase to mission
                    point2d = Point2D(f'P_{f["order"]}.{i}', Latitude(pts[1]), Longitude(pts[0]))
                    wp = Waypoint(f'Waypoints_{int(f["order"]):02d}', point2d, speed=Speed(f["speed"]), depth=f["depth"])
                    mission.add_waypoint(wp)
                    # l_point.append(point2d)
                # waypoints = Waypoints(f'Waypoints_{int(f["order"]):02d}', points=l_point, speed=Speed(f["speed"]), depth=f["depth"])
                # mission.add_waypoints(waypoints)
        if len(Ltrack) > 0:
            mission.add_path(Path("Path", tracks=Ltrack))

      
        # Write json mission file
        # jsonfilepath = os.path.join(OUTPUT_DIR, name+'.json')
        mission.writetofile(filename)
        return


        