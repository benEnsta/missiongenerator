# MissionGenerator

Mission generator is a light (in progess) qgis plugin dedicated to generate AUV / USV mission. 

## Get Started

### Create a new mission

./help/source/images
![](./help/source/images/new_mission.gif)

### Duplicate a track

![](./help/source/images/duplicate_tracks.gif)

### Reverse line direction

![](./help/source/images/inverse_line_direction.gif)

### Export to json

![](./help/source/images/export_to_json.gif)
