#!/usr/bin/env python
from .coordinate import Coordinate

#-----------------------------------------------------------------------------------------------------------------------
class Latitude(Coordinate):
    def __init__(self, degree=0.0, minute=0.0, second=0.0):
        Coordinate.__init__(self, degree, minute, second)
        self._update()
        return

    def get_hemisphere(self):
        '''
        Returns the hemisphere identifier for the current coordinate
        '''
        return 'N' if self.decimal_degree > 0 else 'S'

    def set_hemisphere(self, hemisphere_str):
        '''
        Given a hemisphere identifier, set the sign of the coordinate to match that hemisphere
        '''
        if hemisphere_str == 'S':
            self.degree = -1 * abs(self.degree)
            self.minute = -1 * abs(self.minute)
            self.second = -1 * abs(self.second)
            self._update()
        elif hemisphere_str == 'N':
            self.degree = abs(self.degree)
            self.minute = abs(self.minute)
            self.second = abs(self.second)
            self._update()
        else:
            raise ValueError('Hemisphere identifier for latitudes must be N or S')