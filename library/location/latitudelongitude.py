#!/usr/bin/env python
from .latitude import Latitude
from .longitude import Longitude

# -----------------------------------------------------------------------------------------------------------------------
class LatitudeLongitude:

    def __init__(self, latitude:Latitude=None, longitude:Longitude=None):
        self.latitude = latitude if (latitude is not None and latitude.__class__ is Latitude) else Latitude()
        self.longitude = longitude if (latitude is not None and longitude.__class__ is Longitude) else Longitude()
        return

    @staticmethod
    def _object_islatitude(object):
        if not isinstance(object, Latitude):
            raise Exception('object {} has to be a Latitude object'.format(object.name))

    @staticmethod
    def _object_islongitude(object):
        if not isinstance(object, Longitude):
            raise Exception('object {} has to be a Longitude object'.format(object.name))

    def __str__(self):
        return 'LatLong = {0} deg | {1} deg'.format(self.latitude.get_dd_value(), self.longitude.get_dd_value())

    # Check if two object LatitudeLongitude1 and LatitudeLongitude2 are identical by type, latitude and longitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self.latitude == other.latitude
                and self.longitude == other.longitude
                )

    def set_position(self, latitude: Latitude, longitude: Longitude):
        self.set_latitude(latitude)
        self.set_longitude(longitude)
        return

    def get_position(self):
        return self.get_latitude(), self.get_longitude()

    def set_position_dd(self, latitude_dd, longitude_dd):
        self.latitude.set_dd_value(latitude_dd)
        self.longitude.set_dd_value(longitude_dd)
        return

    def get_position_dd(self):
        return self.get_latitude().get_dd_value(), self.get_longitude().get_dd_value()

    def set_latitude(self, latitude: Latitude):
        self._object_islatitude(latitude)
        self.latitude = latitude
        return

    def get_latitude(self):
        return self.latitude

    def set_longitude(self, longitude: Longitude):
        self._object_islongitude(longitude)
        self.longitude = longitude
        return

    def get_longitude(self):
        return self.longitude
