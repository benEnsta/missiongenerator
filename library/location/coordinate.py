#!/usr/bin/env python
from abc import ABC, abstractmethod

#-----------------------------------------------------------------------------------------------------------------------
# DMS : Degree Minute Seconde (ex : 45°46'52" N)
# DM : Degree Minutes (45° 46.8666' N)
# DD : Decimal degree (45.7811111° N)

# +- DD.D
# +- DDMM.m
# +-DDMMSS.S

#-----------------------------------------------------------------------------------------------------------------------
class Coordinate(ABC):
    def __init__(self, degree=0.0, minute=0.0, second=0.0):
        self.degree = degree
        self.minute = minute
        self.second = second
        self.decimal_degree = 0.0
        self.decimal_minute = 0.0

    def __str__(self):
        return 'coordinate = {0} deg | {1} minute | {2} second'.format(self.degree, self.minute, self.second)

    # Check if two coordinate Coordinate1 and Coordinate2 are identical by type and value
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self.degree == other.degree
                and self.minute == other.minute
                and self.second == other.second
                )

    def _update(self):
        self.decimal_degree = self.dms2dd(self.degree, self.minute, self.second)
        self.degree, self.minute, self.decimal_minute, self.second = self.dd2dm(self.decimal_degree)
        return

    @staticmethod
    def dms2dd(degree, minute, second):
        return degree + minute/60.0 + second/3600.0

    @staticmethod
    def dd2dm(decimal_degree):
        sign = 1
        if decimal_degree < 0.0:
            sign = -1
        decimal_degree = abs(decimal_degree)
        # Truncate degree to be an integer
        degree = decimal_degree//1
        # Calculate the decimal minutes
        degree_minute = (decimal_degree - degree)*60.0
        # Truncate minute to be an integer
        minute = degree_minute//1
        # Calculate the decimal seconds
        second = (degree_minute - minute)*60.0
        # Finally, re-impose the appropriate sign
        degree = sign*degree
        degree_minute = sign*degree_minute
        minute = sign*minute
        second = sign*second
        return degree, minute, degree_minute, second

    def set_dd_value(self, decimal_degree):
        self.degree = decimal_degree
        self.minute = 0
        self.second = 0
        self._update()
        return

    def get_dd_value(self):
        return self.decimal_degree

    def set_dms_value(self, degree, minute, second):
        self.degree = degree
        self.minute = minute
        self.second = second
        self._update()
        return

    def get_dms_value(self):
        return self.degree, self.minute, self.second

    def set_dm_value(self, degree, decimal_minute):
        self.degree = degree
        self.minute = decimal_minute
        self.second = 0.0
        self._update()
        return

    def get_dm_value(self):
        return self.degree, self.decimal_minute

    @abstractmethod
    def get_hemisphere(self):
        '''
        Dummy method, used in child classes such as Latitude and Longitude
        '''
        pass

    @abstractmethod
    def set_hemisphere(self):
        '''
        Dummy method, used in child classes such as Latitude and Longitude
        '''
        pass

    def to_string(self, format_str):
        '''
        Output lat, lon coordinates as string in chosen format
        Inputs:
            format (str) - A string of the form A%B%C where A, B and C are identifiers.
              Unknown identifiers (e.g. ' ', ', ' or '_' will be inserted as separators
              in a position corresponding to the position in format.
        Examples:
            >> palmyra = LatLon(5.8833, -162.0833)
            >> palmyra.to_string('D') # Degree decimal output
            ('5.8833', '-162.0833')
            >> palmyra.to_string('H% %D')
            ('N 5.8833', 'W 162.0833')
            >> palmyra.to_string('d%_%M')
            ('5_52.998', '-162_4.998')
        '''
        format2value = {'H': self.get_hemisphere(),
                        'm': abs(self.decimal_minute),
                        'M': int(abs(self.minute)),
                        'D': int(self.degree),
                        'd': self.decimal_degree,
                        'S': abs(self.second)}
        format_elements = format_str.split('%')
        coord_list = [str(format2value.get(element, element)) for element in format_elements]
        coord_str = ''.join(coord_list)
        # No negative values when hemispheres are indicated
        if 'H' in format_elements:
            coord_str = coord_str.replace('-', '')
        return coord_str