#!/usr/bin/env python
from .coordinate import Coordinate

#-----------------------------------------------------------------------------------------------------------------------
class Longitude(Coordinate):
    def __init__(self, degree=0.0, minute=0.0, second=0.0):
        Coordinate.__init__(self, degree, minute, second)
        self.decimal_degree = self.range180()
        self._update()
        return

    def range180(self):
        '''
        Report longitudes using the range -180 to 180.
        '''
        return ((self.decimal_degree + 180) % 360) - 180

    def range360(self):
        '''
        Report longitudes using the range 0 to 360
        '''
        return (self.decimal_degree + 360) % 360

    def get_hemisphere(self):
        '''
        Returns the hemisphere identifier for the current coordinate
        '''
        return 'E' if self.decimal_degree > 0 else 'W'

    def set_hemisphere(self, hemisphere_str):
        '''
        Given a hemisphere identifier, set the sign of the coordinate to match that hemisphere
        '''
        if hemisphere_str == 'W':
            self.degree = -1 * abs(self.degree)
            self.minute = -1 * abs(self.minute)
            self.second = -1 * abs(self.second)
            self._update()
        elif hemisphere_str == 'E':
            self.degree = abs(self.degree)
            self.minute = abs(self.minute)
            self.second = abs(self.second)
            self._update()
        else:
            raise ValueError('Hemisphere identifier for longitude must be W or E')