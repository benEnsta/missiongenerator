#!/usr/bin/env python3
# ----------------------------------------------------------------------------------------------------------------------
import json

# ----------------------------------------------------------------------------------------------------------------------
class JsonSerializable:
    def __init__(self, sort_keys=False):
        self.sort_keys = sort_keys

    def to_json(self, obj):
        """Implement to return json representation of the serializable object"""
        pass

    def from_json(self, dict):
        """Implement to return json representation of the serializable object"""
        pass

    def _serializor(self, obj):
        if isinstance(obj, JsonSerializable):
            return obj.to_json(obj)
        raise TypeError(repr(obj) + " is not serializable !")

    def writetofile(self, filepath):
        with open(filepath, "w", encoding="utf-8") as jsonfile:
            json.dump(self, jsonfile, indent=4, sort_keys=self.sort_keys, default=self._serializor)
        return

    def _unserializor(self, dict):
        return self.from_json(dict)

    def updatefromfile(self, filepath):
        with open(filepath) as jsonfile:
            dict_from_json = json.load(jsonfile)
            self.from_json(dict_from_json)
        return
# ----------------------------------------------------------------------------------------------------------------------
class JsonSerializables(JsonSerializable):
    def __init__(self, serializables):
        # List of serializable
        self.serializables = serializables

    def get_serialization_object(self, obj):
        if isinstance(obj, JsonSerializables):
            return obj.serializables