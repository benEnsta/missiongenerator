#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
from .common import *
from .phases.missionphase import MissionPhase

from .phases.ascent import Ascent
from .phases.circle import Circle
from .phases.dive import Dive
from .phases.waypoint import Waypoint
from .phases.waypoints import Waypoints
from .phases.track import Track
from .phases.path import Path

# ----------------------------------------------------------------------------------------------------------------------
class Mission(MissionPhase):
    def __init__(self, name=''):
        MissionPhase.__init__(self, MISSION_PARAMS, name)
        self.mission_phases = []

    def __str__(self):
        return 'Mission: id={0} | name={1}'.format(self.get_name(), self.get_id())

    # Check if two point Point1 and Point2 are identical by type, latitude and longitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self._type == other.get_type()
                and self._id == other.get_id()
                and self._name == other.get_name()
                )

    def get_phases(self):
        return self.mission_phases

    def add_ascent(self, ascent: Ascent):
        if not isinstance(ascent, Ascent):
            raise Exception('Object has to be an Asent object')
        self.mission_phases.append(ascent)
        return

    def add_circle(self, circle: Circle):
        if not isinstance(circle, Circle):
            raise Exception('Object has to be a Circle object')
        self.mission_phases.append(circle)
        return

    def add_dive(self, dive: Dive):
        if not isinstance(dive, Dive):
            raise Exception('Object has to be a Dive object')
        self.mission_phases.append(dive)
        return

    def add_path(self, path: Path):
        if not isinstance(path, Path):
            raise Exception('Object has to be a Path object')
        self.mission_phases.append(path)
        return

    def add_track(self, track: Track):
        if not isinstance(track, Track):
            raise Exception('Object has to be a Track object')
        self.mission_phases.append(track)
        return

    def add_waypoint(self, waypoint: Waypoint):
        if not isinstance(waypoint, Waypoint):
            raise Exception('Object has to be a Waypoint object')
        self.mission_phases.append(waypoint)
        return

    def add_waypoints(self, waypoints: Waypoints):
        if not isinstance(waypoints, Waypoints):
            raise Exception('Object has to be a Waypoints object')
        self.mission_phases.append(waypoints)
        return

    def get_setpoints(self):
        return

    def to_json(self, obj):
        json_object = {'type': obj.get_type(),
                       'id': obj.get_id(),
                       'name': obj.get_name(),
                       'phases': obj.mission_phases
                       }
        return json_object

    def from_json(self, dict):
        if dict and dict['type'] == MISSION_PARAMS[0]:
            self.set_name(dict['name'])
            if 'phases' in dict:
                for dict_phase in dict['phases']:
                    if dict_phase['id'] == ASCENT_PARAMS[1]:
                        mission_phase = Ascent()
                    elif dict_phase['id'] == CIRCLETRACK_PARAMS[1]:
                        mission_phase = Circle()
                    elif dict_phase['id'] == DIVE_PARAMS[1]:
                        mission_phase = Dive()
                    elif dict_phase['id'] == TRACK_PARAMS[1]:
                        mission_phase = Track()
                    elif dict_phase['id'] == PATH_PARAMS[1]:
                        mission_phase = Path()
                    elif dict_phase['id'] == WAYPOINT_PARAMS[1]:
                        mission_phase = Waypoint()
                    elif dict_phase['id'] == WAYPOINTS_PARAMS[1]:
                        mission_phase = Waypoints()
                    else:
                        raise Exception('Unknown phase type in mission file')
                    mission_phase.from_json(dict_phase)
                    self.mission_phases.append(mission_phase)
        return
