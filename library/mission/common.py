# Mission object fixed parameters
POINT2D_PARAMS = ['Point2D', 1]
POINT3D_PARAMS = ['Point3D', 2]
SEGMENT_PARAMS = ['Segment', 3]

# Mission phase fixed parameters
ASCENT_PARAMS = ['Ascent', 10]
CIRCLETRACK_PARAMS = ['Circle_Track', 11]
DIVE_PARAMS = ['Dive', 12]
TRACK_PARAMS = ['Track', 13]
PATH_PARAMS = ['Path', 14]
WAYPOINT_PARAMS = ['Waypoint', 15]
WAYPOINTS_PARAMS = ['Waypoints', 16]

# Mission fixed parameters
MISSION_PARAMS = ['Mission', 30]



