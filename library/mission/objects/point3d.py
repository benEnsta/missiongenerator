#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
from .point2d import Point2D
from .missionobject import MissionObject
from ..common import *

# ----------------------------------------------------------------------------------------------------------------------
class Point3D(Point2D):
    def __init__(self, name='', latitude=None, longitude=None, altitude=0.0):
        Point2D.__init__(self, name, latitude, longitude)
        MissionObject.__init__(self, POINT3D_PARAMS, name)
        self.altitude = altitude

    def __str__(self):
        return '{0} = {1} deg | {2} deg | {3} m'.format(self._name, self.latitude.get_dd_value(),
                                                        self.longitude.get_dd_value(), self.altitude)

    # Check if two points P and P2 are identical by type, latitude, longitude and altitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self._type == other._type
                and self._id == other._id
                and self._name == other.name
                and self.latitude.get_dd_value() == other.latitude.get_dd_value()
                and self.longitude.get_dd_value() == other.longitude.get_dd_value()
                and self.altitude == other.altitude
                )

    def set_altitude(self, altitude):
        self.altitude = altitude
        return

    def get_altitude(self):
        return self.altitude

    def to_json(self, obj):
        json_object = {'type': obj._type,
                      'id': obj._id,
                      'name': obj._name,
                      'latitude': obj.latitude.get_dd_value(),
                      'longitude': obj.longitude.get_dd_value(),
                      'altitude': obj.get_altitude()
                      }
        return json_object

    def from_json(self, dict):
        if dict['type'] == 'Point3D':
            self._id = dict['id']
            self.set_name(dict['name'])
            self.latitude.set_dd_value(dict['latitude'])
            self.longitude.set_dd_value(dict['longitude'])
            self.set_altitude(dict['altitude'])
        return