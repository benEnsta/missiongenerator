#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
from .missionobject import MissionObject
from ..common import *
from ...location.latitudelongitude import LatitudeLongitude

# ----------------------------------------------------------------------------------------------------------------------
class Point2D(LatitudeLongitude, MissionObject):
    def __init__(self, name='', latitude=None, longitude=None):
        LatitudeLongitude.__init__(self, latitude, longitude)
        MissionObject.__init__(self, POINT2D_PARAMS, name)

    def __str__(self):
        return '{0} = {1} deg | {2} deg'.format(self._name, self.latitude.get_dd_value(), self.longitude.get_dd_value())

    # Check if two point Point1 and Point2 are identical by type, latitude and longitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self._type == other._type
                and self._id == other._id
                and self._name == other._name
                and self.latitude.get_dd_value() == other.latitude.get_dd_value()
                and self.longitude.get_dd_value() == other.longitude.get_dd_value()
                )

    def to_json(self, obj):
        json_object = {'type': obj._type,
                      'id': obj._id,
                      'name': obj._name,
                      'latitude': obj.latitude.get_dd_value(),
                      'longitude': obj.longitude.get_dd_value()
                      }
        return json_object

    def from_json(self, dict):
        if dict and dict['type'] == 'Point2D':
            self.set_name(dict['name'])
            self.latitude.set_dd_value(dict['latitude'])
            self.longitude.set_dd_value(dict['longitude'])
        return
