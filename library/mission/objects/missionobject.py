#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
from abc import ABC, abstractmethod
from ...common.jsonserializable import JsonSerializable

# ----------------------------------------------------------------------------------------------------------------------
class MissionObject(ABC, JsonSerializable):
    def __init__(self, parameters=[], name=''):
        self._type = parameters[0] if parameters else ''
        self._id = parameters[1] if parameters else -1
        self._name = name
        JsonSerializable.__init__(self)

    @abstractmethod
    def __str__(self):
        pass

    @abstractmethod
    # Check if two point Point1 and Point2 are identical by type, latitude and longitude
    def __eq__(self, other):
        pass

    @abstractmethod
    def to_json(self, obj):
        pass

    @abstractmethod
    def from_json(self, dict):
        pass

    def get_type(self):
        return self._type

    def get_id(self):
        return self._id

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name
