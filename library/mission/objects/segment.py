#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
from .point2d import Point2D
from .missionobject import MissionObject
from ..common import *

# ----------------------------------------------------------------------------------------------------------------------
class Segment(MissionObject):
    def __init__(self, name='', start_point=None, end_point=None):
        MissionObject.__init__(self, SEGMENT_PARAMS, name)
        self.start_point = start_point if (start_point is not None and start_point.__class__ is Point2D) else Point2D()
        self.end_point = end_point if (end_point is not None and end_point.__class__ is Point2D) else Point2D()

    def __str__(self):
        return '{0} = {1} m/s | {2} m | start_point: {3} | end_point: {4}'.format(self.name, self.speed.get_value(), self.altitude, self.start_point, self.end_point)

    # Check if two point Point1 and Point2 are identical by type, latitude and longitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self._type == other._type
                and self._id == other._id
                and self._name == other._name
                and self.start_point == other.start_point
                and self.end_point == other.end_point
                )

    @staticmethod
    def _object_ispoint2D(object):
        if not isinstance(object, Point2D):
            raise Exception('object {} has to be a Longitude object'.format(object.name))

    def set_start_point(self, point):
        self._object_ispoint2D(point)
        self.start_point = point
        return

    def get_start_point(self):
        return self.start_point

    def set_end_point(self, point):
        self._object_ispoint2D(point)
        self.end_point = point
        return

    def get_end_point(self):
        return self.end_point

    def get_points(self):
        start_point = self.get_start_point()
        end_point = self.get_end_point()
        return start_point, end_point

    def to_json(self, obj):
        json_object = {'type': obj._type,
                      'id': obj._id,
                      'name': obj._name,
                      'start_point': obj.start_point.to_json(obj.start_point),
                      'end_point': obj.end_point.to_json(obj.end_point)
                      }
        return json_object

    def from_json(self, dict):
        if dict and dict['type'] == 'Segment':
            self.set_name(dict['name'])
            self.start_point.from_json(dict['start_point'])
            self.end_point.from_json(dict['end_point'])
        return
