#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
from .missionphase import MissionPhase
from ..common import *
from ..objects.point2d import Point2D
from ...positioning.speed import Speed

# ----------------------------------------------------------------------------------------------------------------------
class Circle(MissionPhase):
    def __init__(self, name='', point=None, speed=None, radius=0.0, depth=0.0, altitude=0.0):
        MissionPhase.__init__(self, CIRCLETRACK_PARAMS, name)
        self.point = point if (point is not None and point.__class__ is Point2D) else Point2D()
        self.radius = radius
        self.speed = speed if (speed is not None and speed.__class__ is Speed) else Speed()
        self.depth = depth
        self.altitude = altitude

    def __str__(self):
        string = ''
        if self.altitude == 0.0 and self.depth != 0.0:
            string = '{0} = {1} m/s | depth: {2} m | point: {3}'.format(self.get_name(), self.speed.get_value(), self.depth, self.point)
        if self.depth == 0.0 and self.altitude != 0.0:
            string = '{0} = {1} m/s | altitude : {2} m | point: {3}'.format(self.get_name(), self.speed.get_value(), self.altitude, self.point)
        return string

    # Check if two point Point1 and Point2 are identical by type, latitude and longitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self._type == other.get_type()
                and self._id == other.get_id()
                and self._name == other.get_name()
                and self.point == other.point
                and self.speed == other.speed
                and self.radius == other.radius
                and self.depth == other.depth
                and self.altitude == other.altitude
                )

    def set_point(self, point):
        if not isinstance(point, Point2D):
            raise TypeError('point has to be Point2D object')
        self.point = point
        return

    def get_point(self):
        return self.point

    def set_speed(self, speed):
        if not isinstance(speed, Speed):
            raise TypeError('speed has to be a Speed object')
        self.speed = speed
        return

    def get_speed(self):
        return self.speed

    def set_radius(self, radius):
        self.radius = radius
        return

    def get_radius(self):
        return self.radius

    def set_altitude(self, altitude):
        self.altitude = altitude
        return

    def get_altitude(self):
        return self.altitude

    def set_depth(self, depth):
        self.depth = depth
        return

    def get_depth(self):
        return self.depth

    def get_setpoints(self):
        depth = self.get_depth()
        altitude = self.get_altitude()
        speed = self.get_speed()
        point = self.get_point()
        radius = self.get_radius()
        if self.depth == 0.0 and self.altitude != 0.0:
            return {'altitude': altitude, 'speed': speed, 'point': point, 'radius': radius}
        return {'depth': depth, 'speed': speed, 'point': point, 'radius': radius}

    def to_json(self, obj):
        json_object = {}
        if self.altitude == 0.0 and self.depth != 0.0:
            json_object = {'type': obj.get_type(),
                           'id': obj.get_id(),
                           'name': obj.get_name(),
                           'speed': obj.speed.get_value(),
                           'radius': obj.get_radius(),
                           'depth': obj.get_depth(),
                           'point': obj.point.to_json(obj.point),
                          }
        if self.depth == 0.0 and self.altitude != 0.0:
            json_object = {'type': obj.get_type(),
                           'id': obj.get_id(),
                           'name': obj.get_name(),
                           'speed': obj.speed.get_value(),
                           'radius': obj.get_radius(),
                           'altitude': obj.get_altitude(),
                           'point': obj.point.to_json(obj.point),
                          }
        return json_object

    def from_json(self, dict):
        if dict and dict['type'] == CIRCLETRACK_PARAMS[0]:
            self.set_name(dict['name'])
            self.speed.set_value(dict['speed'])
            self.point.from_json(dict['point'])
            self.set_radius(dict['radius'])
            if 'depth' in dict:
                self.set_depth(dict['depth'])
            if 'altitude' in dict:
                self.set_altitude(dict['altitude'])
        return
