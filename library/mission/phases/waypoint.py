#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
from .missionphase import MissionPhase
from ..common import *
from ..objects.point2d import Point2D
from ...positioning.speed import Speed

# ----------------------------------------------------------------------------------------------------------------------
class Waypoint(MissionPhase):
    def __init__(self, name='', point=None, speed=None, depth=0.0):
        MissionPhase.__init__(self, WAYPOINT_PARAMS, name)
        self.point = point if (point is not None and point.__class__ is Point2D) else Point2D()
        self.speed = speed if (speed is not None and speed.__class__ is Speed) else Speed()
        self.depth = depth

    def __str__(self):
        return '{0} = {1} m/s | {2} m | point: {3}'.format(self._name, self.speed.get_value(), self.depth, self.point)

    # Check if two point Point1 and Point2 are identical by type, latitude and longitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self._type == other.get_type()
                and self._id == other.get_id()
                and self._name == other.get_name()
                and self.point == other.point
                and self.speed == other.speed
                and self.depth == other.depth
                )

    def set_point(self, point):
        if not isinstance(point, Point2D):
            raise TypeError('point has to be Point2D object')
        self.point = point
        return

    def get_point(self):
        return self.point

    def set_speed(self, speed):
        if not isinstance(speed, Speed):
            raise TypeError('speed has to be a Speed object')
        self.speed = speed
        return

    def get_speed(self):
        return self.speed

    def set_depth(self, depth):
        self.depth = depth
        return

    def get_depth(self):
        return self.depth

    def get_setpoints(self):
        depth = self.get_depth()
        speed = self.get_speed()
        point = self.get_point()
        return {'depth': depth, 'speed': speed, 'point': point}

    def to_json(self, obj):
        json_object = {'type': obj.get_type(),
                       'id': obj.get_id(),
                       'name': obj.get_name(),
                       'depth': obj.depth,
                       'speed': obj.speed.get_value(),
                       'point': obj.point.to_json(obj.point)
                       }
        return json_object

    def from_json(self, dict):
        if dict and dict['type'] == WAYPOINT_PARAMS[0]:
            self.set_name(dict['name'])
            self.speed.set_value(dict['speed'])
            self.set_depth(dict['depth'])
            self.point.from_json(dict['point'])
        return