#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
from .missionphase import MissionPhase
from .waypoint import Waypoint
from ..common import *
from ..objects.point2d import Point2D
from ...positioning.speed import Speed

# ----------------------------------------------------------------------------------------------------------------------
class Waypoints(MissionPhase):
    def __init__(self, name='', waypoints=None, points=None, speed=None, depth=0.0):
        MissionPhase.__init__(self, WAYPOINTS_PARAMS, name)
        self.waypoints = waypoints if (waypoints is not None and waypoints.__class__ is list) else []
        self.points = points if (points is not None and points.__class__ is list) else []
        self.speed = speed if (speed is not None and speed.__class__ is Speed) else Speed()
        self.depth = depth

    def __str__(self):
        string = ''
        if self.waypoints and not self.points:
            string = '{0} = waypoints: {1} '.format(self.get_name(), self.waypoints)
        if self.points and not self.waypoints:
            string = '{0} = {1} m/s | {2} m | points: {3}'.format(self.get_name(), self.speed.get_value(), self.depth, self.points)
        return string

    # Check if two point Point1 and Point2 are identical by type, latitude and longitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self._type == other._type
                and self._id == other._id
                and self._name == other._name
                and self.waypoints == other.waypoints
                and self.points == other.points
                and self.speed == other.speed
                and self.depth == other.depth
                )

    def get_waypoints(self):
        return self.waypoints

    def get_waypoint(self, name):
        for waypoint in self.waypoints:
            if waypoint.get_name() == name:
                return waypoint
        return Waypoint()

    def get_points(self):
        return self.points

    def get_point(self, name):
        for point in self.points:
            if point.get_name() == name:
                return point
        return Point2D()

    def set_speed(self, speed):
        if not isinstance(speed, Speed):
            raise TypeError('speed has to be a Speed object')
        self.speed = speed
        return

    def get_speed(self):
        return self.speed

    def set_depth(self, depth):
        self.depth = depth
        return

    def get_depth(self):
        return self.depth

    def get_setpoints(self):
        depth = self.get_depth()
        speed = self.get_speed()
        points = self.get_points()
        waypoints = self.get_waypoints()
        if waypoints:
            return {'waypoints': waypoints}
        return {'depth': depth, 'speed': speed, 'points': points}

    def to_json(self, obj):
        json_object = {}
        if self.waypoints and not self.points:
            json_object = {'type': obj.get_type(),
                           'id': obj.get_id(),
                           'name': obj.get_name(),
                           'waypoints': obj.waypoints
                          }
        if self.points and not self.waypoints:
            json_object = {'type': obj.get_type(),
                           'id': obj.get_id(),
                           'name': obj.get_name(),
                           'depth': obj.get_depth(),
                           'speed': obj.speed.get_value(),
                           'points': obj.points
                           }
        return json_object

    def from_json(self, dict):
        if dict and dict['type'] == WAYPOINTS_PARAMS[0]:
            self.set_name(dict['name'])
            if 'waypoints' in dict:
                for dict_waypoint in dict['waypoints']:
                    waypoint = Waypoint()
                    waypoint.from_json(dict_waypoint)
                    self.waypoints.append(waypoint)
            if 'points' in dict:
                self.set_depth(dict['depth'])
                self.speed.set_value(dict['speed'])
                for dict_point in dict['points']:
                    point = Point2D()
                    point.from_json(dict_point)
                    self.points.append(point)
        return