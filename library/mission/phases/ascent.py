#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
from .missionphase import MissionPhase
from ..common import *

# ----------------------------------------------------------------------------------------------------------------------
class Ascent(MissionPhase):
    def __init__(self, name=''):
        MissionPhase.__init__(self, ASCENT_PARAMS, name)
        self.depth = 0.0

    def __str__(self):
        return '{0} = {1} m'.format(self.get_name(), self.get_depth())

    # Check if two point Point1 and Point2 are identical by type, latitude and longitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self._type == other.get_type()
                and self._id == other.get_id()
                and self._name == other.get_name()
                and self.depth == other.depth
                )

    def set_depth(self, depth):
        self.depth = depth

    def get_depth(self):
        return self.depth

    def get_setpoints(self):
        depth = self.get_depth()
        return {'depth': depth}

    def to_json(self, obj):
        json_object = {'type': obj.get_type(),
                       'id': obj.get_id(),
                       'name': obj.get_name(),
                       }
        return json_object

    def from_json(self, dict):
        if dict and dict['type'] == ASCENT_PARAMS[0]:
            self.set_name(dict['name'])
        return