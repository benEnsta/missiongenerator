#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
from .missionphase import MissionPhase
from .track import Track
from ..common import *
from ..objects.segment import Segment
from ...positioning.speed import Speed

# ----------------------------------------------------------------------------------------------------------------------
class Path(MissionPhase):
    def __init__(self, name='', tracks=None, segments=None, speed=None, altitude=0.0):
        MissionPhase.__init__(self, PATH_PARAMS, name)
        self.tracks = tracks if (tracks is not None and tracks.__class__ is list) else []
        self.segments = segments if (segments is not None and segments.__class__ is list) else []
        self.speed = speed if (speed is not None and speed.__class__ is Speed) else Speed()
        self.altitude = altitude

    def __str__(self):
        string = ''
        if self.tracks:
            string = '{0} = tracks: {1} '.format(self._name, self.tracks)
        if self.segments:
            string = '{0} = {1} m/s | {2} m | rails: {3}'.format(self._name, self.speed.get_value(), self.altitude, self.segments)
        return string

    # Check if two point Point1 and Point2 are identical by type, latitude and longitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self._type == other.get_type()
                and self._id == other.get_id()
                and self._name == other.get_name()
                and self.tracks == other.tracks
                and self.segments == other.segments
                and self.speed == other.speed
                and self.altitude == other.altitude
                )

    def get_tracks(self):
        return self.tracks

    def get_track(self, name):
        for track in self.tracks:
            if track.get_name() == name:
                return track
        return Track()

    def get_segments(self):
        return self.segments

    def get_segment(self, name):
        for segment in self.segments:
            if segment.get_name() == name:
                return segment
        return Segment()

    def set_speed(self, speed):
        if not isinstance(speed, Speed):
            raise TypeError('speed has to be a Speed object')
        self.speed = speed
        return

    def get_speed(self):
        return self.speed

    def set_altitude(self, altitude):
        self.altitude = altitude
        return

    def get_altitude(self):
        return self.altitude

    def get_setpoints(self):
        altitude = self.get_altitude()
        speed = self.get_speed()
        segments = self.get_segments()
        tracks = self.get_tracks()
        if tracks:
            return {'tracks': tracks}
        return {'altitude': altitude, 'speed': speed, 'segments': segments}

    def to_json(self, obj):
        json_object = {}
        if self.tracks:
            json_object = {'type': obj.get_type(),
                           'id': obj.get_id(),
                           'name': obj.get_name(),
                           'tracks': obj.tracks
                           }
        if self.segments:
            json_object = {'type': obj.get_type(),
                           'id': obj.get_id(),
                           'name': obj.get_name(),
                           'altitude': obj.altitude,
                           'speed': obj.speed.get_value(),
                           'segments': obj.segments
            }
        return json_object

    def from_json(self, dict):
        if dict and dict['type'] == PATH_PARAMS[0]:
            self.set_name(dict['name'])
            if 'tracks' in dict:
                for dict_track in dict['tracks']:
                    track = Track()
                    track.from_json(dict_track)
                    self.tracks.append(track)
            if 'segments' in dict:
                self.set_altitude(dict['altitude'])
                self.speed.set_value(dict['speed'])
                for dict_segment in dict['segments']:
                    segment = Segment()
                    segment.from_json(dict_segment)
                    self.segments.append(segment)
        return