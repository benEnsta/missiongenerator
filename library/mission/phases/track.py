#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
from .missionphase import MissionPhase
from ..common import *
from ..objects.segment import Segment
from ...positioning.speed import Speed

# ----------------------------------------------------------------------------------------------------------------------
class Track(Segment, MissionPhase):
    def __init__(self, name='', start_point=None, end_point=None, speed=None, altitude=0.0):
        Segment.__init__(self, name, start_point, end_point)
        MissionPhase.__init__(self, TRACK_PARAMS, name)
        self.speed = speed if (speed is not None and speed.__class__ is Speed) else Speed()
        self.altitude = altitude

    def __str__(self):
        return '{0} = {1} m/s | {2} m | start_point: {3} | end_point: {4}'.format(self._name, self.speed.get_value(), self.altitude, self.start_point, self.end_point)

    # Check if two point Point1 and Point2 are identical by type, latitude and longitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self._type == other.get_type()
                and self._id == other.get_id()
                and self._name == other.get_name()
                and self.start_point == other.start_point
                and self.end_point == other.end_point
                and self.speed == other.speed
                and self.altitude == other.altitude
                )

    def set_speed(self, speed):
        if not isinstance(speed, Speed):
            raise TypeError('speed has to be a Speed object')
        self.speed = speed
        return

    def get_speed(self):
        return self.speed

    def set_altitude(self, altitude):
        self.altitude = altitude
        return

    def get_altitude(self):
        return self.altitude

    def get_setpoints(self):
        altitude = self.get_altitude()
        speed = self.get_speed()
        points = [self.get_start_point(), self.get_end_point()]
        return {'altitude': altitude, 'speed': speed, 'points': points}

    def to_json(self, obj):
        json_object = {'type': obj.get_type(),
                       'id': obj.get_id(),
                       'name': obj.get_name(),
                       'altitude': obj.altitude,
                       'speed': obj.speed.get_value(),
                       'start_point': obj.start_point.to_json(obj.start_point),
                       'end_point': obj.end_point.to_json(obj.end_point)
                       }
        return json_object

    def from_json(self, dict):
        if dict and dict['type'] == TRACK_PARAMS[0]:
            self.set_name(dict['name'])
            self.speed.set_value(dict['speed'])
            self.set_altitude(dict['altitude'])
            self.start_point.from_json(dict['start_point'])
            self.end_point.from_json(dict['end_point'])
        return