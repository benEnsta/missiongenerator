#!/usr/bin/env python
#---------------------------------------------------------------------------------------------------------------------
from .distance import Distance

#---------------------------------------------------------------------------------------------------------------------
class PointXY(object):
    def __init__(self, name='', x=None, y=None):
        self.name = name
        self.x = x if x is not None else Distance()
        self.y = y if y is not None else Distance()

    def __str__(self):
        return '{0} = {1} m | {2} m'.format(self.name, self.x, self.y)

    # Check if two point PointXY1 and PointXY2 are identical by type, x and y
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self.x == other.x
                and self.y == other.y
                )

    @staticmethod
    def _object_isdistance(object):
        if object.__class__ is not Distance:
            raise Exception('object {} has to be a Distance object'.format(object.name))

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def set_distances(self, x, y):
        self.set_x(x)
        self.set_y(y)
        return

    def get_distances(self):
        return self.get_x(), self.get_y()

    def set_values(self, x, y):
        self.x.set_value(x)
        self.y.set_value(y)
        return

    def get_values(self):
        return self.x.get_value(), self.y.get_value()

    def set_x(self, x):
        self._object_isdistance(x)
        self.x = x
        return

    def get_x(self):
        return self.x

    def set_y(self, y):
        self._object_isdistance(y)
        self.y = y
        return

    def get_y(self):
        return self.y