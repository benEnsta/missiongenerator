#!/usr/bin/env python
from math import degrees, radians

#-----------------------------------------------------------------------------------------------------------------------
class Angle(object):
    def __init__(self, value=0.0, rad=True):
        if not rad:
            self.value = radians(value)
        else:
            self.value = value
        return

    def __str__(self):
        return 'angle = {0} rad | {1} deg'.format(self.value, degrees(self.value))

    # Check if two angle Angle1 and Angle2 are identical by type and value
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self.value == other.value
                )

    def set_value(self, value_rad):
        self.value = value_rad
        return

    def set_value_deg(self, value_deg):
        self.value = radians(value_deg)
        return

    def get_value(self):
        return self.value

    def get_value_deg(self):
        return degrees(self.value)
