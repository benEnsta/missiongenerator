#!/usr/bin/env python
from .angle import Angle

#---------------------------------------------------------------------------------------------------------------------
class Attitude(object):
    def __init__(self, pitch=None, roll=None, yaw=None):
        self.pitch = pitch if pitch is not None else Angle()
        self.roll = roll if roll is not None else Angle()
        self.yaw = yaw if yaw is not None else Angle()

    def __str__(self):
        return 'attitude: pitch: {0} ; roll: {1} ; yaw: {2}'.format(self.pitch, self.roll, self.yaw)

    # Check if two attitude Attitude1 and Attitude2 are identical by type, pitch, roll and yaw
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self.pitch == other.pitch
                and self.roll == other.roll
                and self.yaw == other.yaw
                )

    @staticmethod
    def _object_isangle(object):
        if object.__class__ is not Angle:
            raise Exception('object {} has to be an Angle object'.format(object.name))

    def set_angles(self, pitch:Angle, roll:Angle, yaw:Angle):
        self.set_pitch(pitch)
        self.set_roll(roll)
        self.set_yaw(yaw)
        return

    def get_anles(self):
        return self.get_pitch(), self.get_roll(), self.get_yaw()

    def set_values(self, pitch, roll, yaw):
        self.pitch.set_value(pitch)
        self.roll.set_value(roll)
        self.yaw.set_value(yaw)
        return

    def get_values(self):
        return self.pitch.get_value(), self.roll.get_value(), self.yaw.get_value()

    def set_values_deg(self, pitch_deg, roll_deg, yaw_deg):
        self.pitch.set_value_deg(pitch_deg)
        self.roll.set_value_deg(roll_deg)
        self.yaw.set_value_deg(yaw_deg)
        return

    def get_values_deg(self):
        return self.pitch.get_value_deg(), self.roll.get_value_deg(), self.yaw.get_value_deg()

    def set_pitch(self, pitch:Angle):
        self._object_isangle(pitch)
        self.pitch = pitch
        return

    def get_pitch(self):
        return self.pitch

    def set_roll(self, roll:Angle):
        self._object_isangle(roll)
        self.roll = roll
        return

    def get_roll(self):
        return self.roll

    def set_yaw(self, yaw:Angle):
        self._object_isangle(yaw)
        self.yaw = yaw
        return

    def get_yaw(self):
        return self.yaw
