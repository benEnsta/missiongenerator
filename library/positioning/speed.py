#!/usr/bin/env python
#-----------------------------------------------------------------------------------------------------------------------
from ..common.jsonserializable import JsonSerializable

#-----------------------------------------------------------------------------------------------------------------------
# CONSTANTS
KILOMETER_H_TO_METER_S = 0.277778
KNOTS_TO_METER_S = 0.514444
MILES_H_TO_METER_S = 0.44704
FEET_S_TO_METER_S = 0.3048

#-----------------------------------------------------------------------------------------------------------------------
class Speed(JsonSerializable):
    def __init__(self, value=0.0):
        JsonSerializable.__init__(self)
        self.value = float(value)

    def __str__(self):
        return 'speed = {0} m/s'.format(self.value)

    # Check if two speed Speed1 and Speed1 are identical by type and value
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self.value == other.value
                )

    def set_value(self, value):
        self.value = value
        return

    def get_value(self):
        return self.value

    def set_value_mileshour(self, value_miles):
        self.value = value_miles * MILES_H_TO_METER_S
        return

    def get_value_mileshour(self):
        return self.value/MILES_H_TO_METER_S

    def set_value_knots(self, value_nautmiles):
        self.value = value_nautmiles * KNOTS_TO_METER_S
        return

    def get_value_knots(self):
        return self.value/KNOTS_TO_METER_S

    def set_value_feetsecond(self, value_feetsecond):
        self.value = value_feetsecond * FEET_S_TO_METER_S
        return

    def get_value_feetsecond(self):
        return self.value/FEET_S_TO_METER_S

    def set_value_kmhour(self, value_kmhour):
        self.value = value_kmhour * KILOMETER_H_TO_METER_S
        return

    def get_value_kmhour(self):
        return self.value/KILOMETER_H_TO_METER_S

    def to_json(self, obj):
        json_point = {'speed': obj.value}
        return json_point

    def from_json(self, dict):
        self.set_value(dict['speed'])
        return
