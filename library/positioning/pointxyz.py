#!/usr/bin/env python
#---------------------------------------------------------------------------------------------------------------------
from .pointxy import PointXY
from .distance import Distance

#---------------------------------------------------------------------------------------------------------------------
class PointXYZ(PointXY):
    def __init__(self, name='', x=None, y=None, z=None):
        super(PointXYZ, self).__init__(name, x, y)
        self.z = z if z is not None else Distance()

    def __str__(self):
        return '{0} = {1} m | {2} m | {3} m'.format(self.name, self.x, self.y, self.z)

    # Check if two points P and P2 are identical by type, latitude, longitude and altitude
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self.x == other.x
                and self.y == other.y
                and self.z == other.z
                )

    def set_distances(self, x, y, z):
        self.set_x(x)
        self.set_y(y)
        self.set_z(y)
        return

    def get_distances(self):
        return self.get_x(), self.get_y(), self.get_z()

    def set_values(self, x, y, z):
        self.x.set_value(x)
        self.y.set_value(y)
        self.z.set_value(z)
        return

    def get_values(self):
        return self.x.get_value(), self.y.get_value(), self.z.get_value()

    def set_z(self, z):
        self._object_isdistance(z)
        self.z = z
        return

    def get_z(self):
        return self.z
