#!/usr/bin/env python

#-----------------------------------------------------------------------------------------------------------------------
# CONSTANTS
KILOMETER_TO_METER = 1000
NAUTIC_TO_METER = 1852
MILES_TO_METER = 1609.34
FEET_TO_METER = 0.3048

#-----------------------------------------------------------------------------------------------------------------------
class Distance(object):
    def __init__(self, value=0.0):
        self.value = value

    def __str__(self):
        return 'distance = {0} m'.format(self.value)

    # Check if two angle Angle1 and Angle2 are identical by type and value
    def __eq__(self, other):
        return (self.__class__ == other.__class__
                and self.value == other.value
                )

    def set_value(self, value):
        self.value = value
        return

    def get_value(self):
        return self.value

    def set_value_miles(self, value_miles):
        self.value = value_miles * MILES_TO_METER
        return

    def get_value_miles(self):
        return self.value/MILES_TO_METER

    def set_value_nautmiles(self, value_nautmiles):
        self.value = value_nautmiles * NAUTIC_TO_METER
        return

    def get_value_nautmiles(self):
        return self.value/NAUTIC_TO_METER

    def set_value_feet(self, value_feet):
        self.value = value_feet * FEET_TO_METER
        return

    def get_value_feet(self):
        return self.value/FEET_TO_METER

    def set_value_km(self, value_km):
        self.value = value_km * KILOMETER_TO_METER
        return

    def get_value_km(self):
        return self.value/KILOMETER_TO_METER
