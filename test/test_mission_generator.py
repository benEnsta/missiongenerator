# coding=utf-8
"""Dialog test.

.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

"""

__author__ = 'bendesch@hotmail.com'
__date__ = '2019-11-30'
__copyright__ = 'Copyright 2019, Benoit Desrochers'

import unittest

# from qgis.PyQt.QtGui import QDialogButtonBox, QDialog

from mission_generator_dialog import mission_genratorDialog
from mission_generator import MyVectorLayer
import os
from utilities import get_qgis_app
QGIS_APP = get_qgis_app()


class mission_genratorDialogTest(unittest.TestCase):

    def setUp(self):

        # self.vl = MyVectorLayer("/tmp/mylayer2.gpkg", "Mission", "ogr")
        self.vl = MyVectorLayer.newMemoryLayer()
        order_idx = self.vl.fields().indexFromName("order")
        self.vl.startEditing()
        self.vl.dataProvider().truncate()
        # print([f.name() for f in self.vl.fields()])
        f1 = QgsFeature(self.vl.fields())
        geom = QgsGeometry.fromPolylineXY([QgsPointXY(0,0), QgsPointXY(1,1)])
        f1.setGeometry(geom)
        f1.setAttribute(order_idx, 1)
        f1["bearing"] = 90
        f2 = QgsFeature(self.vl.fields())
        f2.setAttribute(order_idx, 2)
        f2["bearing"] = 90
        f2.setGeometry(geom)

        # f2 = QgsFeature(self.vl.fields())
        self.vl.addFeatures([f1, f2])
        self.vl.commitChanges()

        # qml_filename = os.path.join(os.path.dirname(__file__), 'qml_files/style_8.qml')
        # self.vl.loadNamedStyle(qml_filename)

    def test_swapOrder(self):
        features = list(self.vl.getFeatures())

        self.assertEqual(features[0]["order"], 1)
        self.assertEqual(features[1]["order"], 2)
        # print("PRINT AVANT")
        # for f in self.vl.getFeatures():
            # print(f.id(), f.attributes())
        self.vl.startEditing()
        self.vl.swapOrder(features[0], features[1])
        # print("PRINT APRES")

        # for f in self.vl.getFeatures():
            # print(f.id(), f.attributes())

        self.assertEqual(features[0]["order"], 2)
        self.assertEqual(features[1]["order"], 1)


    def test_addFeature(self):

        newFeat = QgsFeature(self.vl.getFeature(1))
        self.vl.startEditing()
        self.vl.addFeatureWithOrder(newFeat)
        self.vl.addFeatureWithOrder(newFeat)
        self.vl.addFeatureWithOrder(newFeat)
        self.vl.addFeatureWithOrder(newFeat)


        order = [f["order"] for f in self.vl.getFeatures()]
        self.assertEqual(list(range(1,7)), order)
        
    def test_insert_first(self):
        # return
        newFeat = QgsFeature(self.vl.getFeature(1))
        self.vl.startEditing()
        order = self.vl.addFeatureWithOrder(newFeat)
        
        # for f in self.vl.getFeatures():
        #     print("before ", f.id(), f.attributes())
        self.vl.moveOrder(order, 1)
        order = [f["order"] for f in self.vl.getFeatures()]
        self.assertEqual([2,3,1], order)

        # for f in self.vl.getFeatures():
        #     print("after ",f.id(), f.attributes())
        pass
    def test_insert_last(self):

        newFeat = QgsFeature(self.vl.getFeature(1))
        self.vl.startEditing()
        order = self.vl.addFeatureWithOrder(newFeat)
        # order = self.vl.addFeatureWithOrder(newFeat)
        # order = self.vl.addFeatureWithOrder(newFeat)
        # order = self.vl.addFeatureWithOrder(newFeat)
        # order = self.vl.addFeatureWithOrder(newFeat)
        # order = self.vl.addFeatureWithOrder(newFeat)
        
        # for f in self.vl.getFeatures():
            # print("before ", f.id(), f.attributes())
        self.vl.moveOrder(1, order)
        order = [f["order"] for f in self.vl.getFeatures()]
        # for f in self.vl.getFeatures():
            # print("after ",f.id(), f.attributes())
        self.assertEqual([3,1,2], order)


        pass
    def test_insert_middle(self):
        newFeat = QgsFeature(self.vl.getFeature(1))
        self.vl.startEditing()
        order = self.vl.addFeatureWithOrder(newFeat)
        order = self.vl.addFeatureWithOrder(newFeat)
        order = self.vl.addFeatureWithOrder(newFeat)
        order = self.vl.addFeatureWithOrder(newFeat)
        # order = self.vl.addFeatureWithOrder(newFeat)
        # order = self.vl.addFeatureWithOrder(newFeat)
        
        # for f in self.vl.getFeatures():
            # print("before ", f.id(), f.attributes())
        self.vl.moveOrder(3, 4)
        order = [f["order"] for f in self.vl.getFeatures()]
        # for f in self.vl.getFeatures():
            # print("after ",f.id(), f.attributes())
        self.assertEqual([1,2,4,3,5,6], order)

        pass

    def test_insert(self):
        newFeat = QgsFeature(self.vl.getFeature(1))
        self.vl.startEditing()
        order = self.vl.addFeatureWithOrder(newFeat)
        order = self.vl.addFeatureWithOrder(newFeat)
        self.vl.insertFeatureAfter(newFeat, 2)
        order = [f["order"] for f in self.vl.getFeatures()]
        self.assertEqual([1,3,4,5,2], order)

    def test_paralle_lines(self):
        self.vl.genParallelLines(1)
        for f in self.vl.getFeatures():
            print("after ",f.id(), f.attributes())

    
    def test_export(self):
        vl = MyVectorLayer("/tmp/layer.gpkg", "Mission", "ogr")
        vl.export()
        

if __name__ == "__main__":

    from qgis.core import *

    # Supply path to qgis install location
    QgsApplication.setPrefixPath("/path/to/qgis/installation", True)

    # Create a reference to the QgsApplication.  Setting the
    # second argument to False disables the GUI.
    qgs = QgsApplication([], False)

    # Load providers
    qgs.initQgis()

    # Write your code here to load some layers, use processing
    # algorithms, etc.
    suite = unittest.makeSuite(mission_genratorDialogTest)
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)

    # Finally, exitQgis() is called to remove the
    # provider and layer registries from memory
    qgs.exitQgis()

